"""
this takes a csv outputted by progenesis QI
and converts it to a consensusxml format
that can act as an input to GraphMS.
"""

from graphms import custom_tools
import pandas as pd
import numpy as np
import os

def get_file(file_path=''):
    if file_path=='':
        file_path = r'O:\Analytics\Lab data backup\Orbitrap Fusion\2019\May\progeneis cho epo triplicates r1,2,3-2, runs separate (profile data).csv'

    df = pd.read_csv(file_path, header=[0,1,2], index_col=None, low_memory=False)
    df.columns = ['_'.join([val for val in triplet if 'Unnamed' not in val]) for triplet in list(df.columns)]

    return df


def generate_consensus_file(df_of_progenesis_compound_measurements):
    # initialise the result dataframe
    result_cho = pd.DataFrame(columns=['rt_cf',	'mz_cf', 'intensity_cf',
                                   'charge_cf','width_cf', 'quality_cf',
                                   'rt_0','mz_0','intensity_0','charge_0'])
    result_hek = result_cho.copy()

    # split for cho and hek, where normalised abundance is nonzero
    df_of_progenesis_compound_measurements_cho = df_of_progenesis_compound_measurements.loc[
                                                 df_of_progenesis_compound_measurements.iloc[:,13]>0,:]
    df_of_progenesis_compound_measurements_hek = df_of_progenesis_compound_measurements.loc[
                                                 df_of_progenesis_compound_measurements.iloc[:, 14] > 0, :]

    result_cho.loc[:, 'rt_cf'] = df_of_progenesis_compound_measurements_cho['Retention time (min)']*60
    result_cho.loc[:, 'mz_cf'] = df_of_progenesis_compound_measurements_cho['Neutral mass (Da)']
    result_cho.loc[:, 'mz_cf'] =  result_cho.loc[:, 'mz_cf'].fillna(df_of_progenesis_compound_measurements_cho['m/z']*df_of_progenesis_compound_measurements_cho['Charge']-(df_of_progenesis_compound_measurements_cho['Charge']*1.00728))

    # this is the line with column depending on an arbitrary number.
    result_cho.loc[:, 'intensity_cf'] = df_of_progenesis_compound_measurements_cho.iloc[:,13]

    result_hek.loc[:, 'rt_cf'] = df_of_progenesis_compound_measurements_hek['Retention time (min)']*60
    result_hek.loc[:, 'mz_cf'] = df_of_progenesis_compound_measurements_hek['Neutral mass (Da)']
    result_hek.loc[:, 'mz_cf'] = result_hek.loc[:, 'mz_cf'].fillna(
        df_of_progenesis_compound_measurements_hek['m/z'] * df_of_progenesis_compound_measurements_hek['Charge'] - (
                df_of_progenesis_compound_measurements_hek['Charge'] * 1.00728))
    result_hek.loc[:, 'intensity_cf'] = df_of_progenesis_compound_measurements_hek.iloc[:, 14]

    return result_cho, result_hek


def get_list_of_columns_that_are_sample_raw_abundances(df_in):
    """
    uses the number of columns between Normalised Abundance and Raw Abundance to detect the sample names
    Use those sample names to collect the correct columns, as integer list (not names because of duplication)
    :param df_in:
    :return:
    """

    first_marker_column_index = np.argwhere(['Normalised abundance' in x for x in df.columns])[0][0]
    last_marker_column_index = np.argwhere(['Raw abundance' in x for x in df.columns])[0][0]
    count_of_columns_with_samples = last_marker_column_index - first_marker_column_index

    list_of_cols = list(range(last_marker_column_index, last_marker_column_index+6))

    return list_of_cols


def save_df_as_csv(df, file_name):
    df.to_csv(file_name, sep=',', na_rep='0', index=None)
    print('Saved dataframe of shape {} to file {}.'.format(df.shape, file_name))



if __name__ == '__main__':
    df = get_file()
    cho, hek = generate_consensus_file(df)
    save_df_as_csv(cho, r'O:\Analytics\Matt\042 EPO christine\May_2019\CHO consensusxml.csv')
    save_df_as_csv(hek, r'O:\Analytics\Matt\042 EPO christine\May_2019\HEK consensusxml.csv')
