"""
desired intermediate output:
1. a list of peptide IDs matched to byonic nodes, which I can then plot in 2D with x’s, overlaying the graphMS output.
using the byonic IDs as reference nodes for GraphMS relative ID.
2. assign a confidence level/score based on:
if MS2 has oxonium and/or pepN
number of similar byonic IDs in the subgraph.
similarity of spectra
if RTs conform to expected patterns.


steps
1. get consensusxml, alignedbyonictocsv file, list of graphmls
2. PHASE ONE:
 2a. on one plot, plot consensusxml nodes, byonic hits, and graphmls (grouped as one). Just use bokehgraphms.
 2b. from here, manually go through and evaluate which byonic hits are reputable, and from that, generate a
        list to use as reference nodes.
 2c. predict the compositions of graphmls based on these reference nodes

3. PHASE TWO:


"""

from graphms import custom_tools, bokehgraphms, cleanconsensus, glycanformat
import networkx as nx
import pandas as pd, numpy as np
import os


class plotoverlays:
    def __init__(self, cons_file, aligned_file, list_of_graphmls):

        # get the input files and convert them to graphmls
        self.cons_file = cons_file
        self.aligned_file = aligned_file
        self.list_of_graphmls = list_of_graphmls

    def convert_cons_file_to_graphms(self, filepath):
        ### this converts input files into graphmls

        # zeroth, clean consensus just in case. this takes and outputs a list, hence the indexing.
        filepath = cleanconsensus.clean([filepath])[0]

        # first, extract to pandas dataframe
        df = pd.read_csv(filepath, header=0,
                         dtype={0: np.float,
                                1: np.float,
                                2: np.float,
                                9: np.int
                                })
        df = custom_tools.checkcolumnsandreplace(df)

        G = nx.Graph()


        # second, pick the correct columns and convert to graph G

        # third, encode as graphml
        pass

    def convert_abscomp_to_graphml(self):
        """
        the idea here is to take an abscomp file,
        sort it into subgraphs based on the refnode_index,
        save it as pickle or graphml.


        :return:
        """
        pass

    def graphml_to_individual_exports(self):
        """
        the idea here is to iterate through the subgraphs of a graphml file,
         plot each one separately, and export as images/htmls into the folder,
         for the user to review.

        :return:
        """
        pass


    def plotoverlays(self):



        # define options for plotting
        options = dict(
            export_csv=False,
            circle_size_max=40,
            circle_size_min=5,
            show_edges=False,
            output_format='canvas',
            style='',
            output_html_file_path='test.html',
            scaling=2,
        )

        # define the plot from the class subgraphfigure
        p = bokehgraphms.subgraphfigure()

        # add graphmls as subgraphs
        p.add_subgraphs_to_plot()


def populate_reference_nodes_with_byonic(predcomp_filepath, ref_filepath, byonicaligned):

    # read the input files
    df_pred = pd.read_csv(predcomp_filepath, index_col='node', header=0)
    df_ref = pd.read_excel(ref_filepath, index_col=0, header=0)
    df_byonicaligned = pd.read_excel(byonicaligned, index_col='node', header=0)

    # add in extra columns that we want to show in the final df (adding them here makes the df.update add them from the aligned byonic file)
    # df_ref['protein'] = np.zeros(len(df_ref.index))
    # df_ref['scan_no'] = np.zeros(len(df_ref.index))
    # df_ref['score'] = np.zeros(len(df_ref.index))

    # 120718 - instead, just add all the columns possible, using the set function to get the unique ones.
    # column_set = set(list(df_ref.columns) + list(df_byonicaligned.columns))
    novel_column_set = [x for x in list(df_byonicaligned.columns) if x not in list(df_ref.columns)]
    for novel_col in novel_column_set:
        df_ref[novel_col] = np.zeros(len(df_ref.index))

    unique_subgraphs = df_pred.loc[:, 'refnode'][~df_pred.loc[:, ['refnode']].duplicated()]

    # align df_pred with df_byonicaligned, based on mz and rt
    # do it on the basis of df_byonic, because it is the smaller set
    def matchbyrtmz(mz, rt, mztol=0.01, rttol=60.):
        # slice to get a list of answers, and pick the closest one

        matchlist = df_pred.loc[np.isclose(mz, df_pred['mz_node'], atol=mztol, rtol=0) & \
                                np.isclose(rt, df_pred['rt_node'], atol=rttol, rtol=0),
                    :]
        if len(matchlist) <= 0: return 0
        matchlist = matchlist.sort_values(by='intensity', ascending=False)
        #        print('\nmatchlist = \n{}'.format(matchlist))
        return matchlist.index[0]

    print('aligning nodes...')

    # get the desired index using the sub-function I defined above.
    # This find the best match for the byonic hits from the predcomp nodes.
    new_index = ([np.vectorize(matchbyrtmz)(df_byonicaligned['mz_cf'], df_byonicaligned['rt_cf'])])
    df_byonicaligned.set_index(new_index, inplace=True, drop=True)

    output_df = pd.DataFrame()

    # cycle through each unique subgraphs and define the reference node based on the byonic matched peptide with lowest pep2D.

    for refnode in unique_subgraphs:
        current_nodelist = df_pred.loc[df_pred['refnode'] == refnode, :]

        try:
            matching_aligned_nodes = df_byonicaligned.loc[current_nodelist.index, :]
        except:
            continue

        # unique_extracted_peptide_ids = matching_aligned_nodes.loc[:, 'pep'][~matching_aligned_nodes[['pep']].duplicated()]
        #        number_of_unique_peptides = len(unique_extracted_peptide_ids.index)

        # count how many for each unique peptide
        grouped_unique_peptides = matching_aligned_nodes.groupby(['peptide']).count()
        print('peptideID and number of matches:\n', grouped_unique_peptides.iloc[:, 0])

        # get the most numerous peptide
        most_numerous_peptide = grouped_unique_peptides.sort_values(by='rt_cf', ascending=False).index[0]

        # get the most abundant node and use that as ref (likely to have ms2)
        top_node = matching_aligned_nodes.loc[
                       matching_aligned_nodes['peptide'] == most_numerous_peptide, :].sort_values(
                                                                                    by='intensity_cf',
                                                                                    ascending=False)[0:1]
        print('pep2D of top node = {}'.format(list(top_node['pep2D'])))
        top_node = pd.concat(
            [top_node,
             pd.DataFrame(glycanformat.glycan_get_comp(list(top_node['glycan'])[0]),
                          index=top_node.index)
             ],
            axis=1,
        )
        target_reference_line = df_ref.loc[df_ref['node'] == refnode, :]
        top_node.set_index(target_reference_line.index, inplace=True)
        top_node.rename(columns={'rt_cf': 'rt_node',
                                 'mz_cf': 'mz_node',
                                 'peptide': 'pep',
                                 },
                        inplace=True,
                        )

        #        pdb.set_trace()
        # assign this to the ref node

        target_reference_line.update(top_node)
        target_reference_line.loc[:, 'node'] = [0]
        target_reference_line.loc[:, 'pep2D'] = list(top_node['pep2D'])[0]
        # target_reference_line.loc[:, 'Score'] = list(top_node['Score'])[0]
        output_df = pd.concat([output_df, target_reference_line], axis=0)
        # pd.DataFrame.combine_first() # the first one overwrites the one in the brackets
        # DataFrame.update(other, join='left', overwrite=True, filter_func=None, raise_conflict=False)

    return output_df


def run_with_filename_prompts():

    # get the filenames from the user
    aligned_file = custom_tools.getfile('Select the aligned byonic to consensus file', '.csv, .xls*')
    ref_file = custom_tools.getfile('Select the reference nodes file', '.xls*')
    predcomp_file = custom_tools.getfile('Select the predicted relative compositions file.', '.csv')

    # (predcomp_filepath, ref_filepath, byonicaligned)
    df = populate_reference_nodes_with_byonic(predcomp_filepath=predcomp_file, ref_filepath=ref_file,
                                              byonicaligned=aligned_file)
    savedir = os.path.dirname(aligned_file)
    pickle_save_file = os.path.join(savedir, 'populated_df.pkl')
    df.to_pickle(pickle_save_file)

    save_path = os.path.join(savedir, 'matched_reference_nodes.xlsx')
    writer = pd.ExcelWriter(save_path)
    df.to_excel(writer)
    writer.save()

    return save_path

def run_with_defined_filepaths(aligned_file, ref_file, predcomp_file, save_path):

    # get the filenames from the user
    # aligned_file = custom_tools.getfile('Select the aligned byonic to consensus file', '.csv, .xls*')
    # ref_file = custom_tools.getfile('Select the reference nodes file', '.xls*')
    # predcomp_file = custom_tools.getfile('Select the predicted relative compositions file.', '.csv')

    # (predcomp_filepath, ref_filepath, byonicaligned)
    df = populate_reference_nodes_with_byonic(predcomp_filepath=predcomp_file, ref_filepath=ref_file,
                                              byonicaligned=aligned_file)
    # savedir = os.path.dirname(aligned_file)
    pickle_save_file = os.path.splitext(save_path)[0] + '.pkl'
    df.to_pickle(pickle_save_file)

    # save_path = os.path.join(savedir, 'matched_reference_nodes.xlsx')
    writer = pd.ExcelWriter(save_path)
    df.to_excel(writer)
    writer.save()

    return save_path

if __name__ == "__main__":
    run_with_filename_prompts()
    # # cons_file = custom_tools.getfile('Select the consensusxml file', '.csv')
    # aligned_file = custom_tools.getfile('Select the aligned byonic to consensus file', '.csv, .xls*')
    # # # list_of_graphmls = custom_tools.getfiles('Select the graphmls', '.graphml')
    # ref_file = custom_tools.getfile('Select the reference nodes file', '.xls*')
    # predcomp_file = custom_tools.getfile('Select the predicted relative compositions file.', '.csv')
    #
    # # aligned_file = r'J:\HGI\round_2\aligned byonic O-glycopeptide to A consensus filtered redundant.xlsx'
    # # ref_file = r'K:\LCMSMS Raw files (use these files for glycopeptide identification)\KNIME\A_graph7_deg2up_neuac55_1e5_50s\predcomp deg2up_reference_nodes_2101out.xlsx'
    # # predcomp_file = r'K:\LCMSMS Raw files (use these files for glycopeptide identification)\KNIME\A_graph7_deg2up_neuac55_1e5_50s\predcomp deg2up_2101out.csv'
    #
    # # (predcomp_filepath, ref_filepath, byonicaligned)
    # df = populate_reference_nodes_with_byonic(predcomp_filepath=predcomp_file, ref_filepath=ref_file, byonicaligned=aligned_file)
    # savedir = os.path.dirname(aligned_file)
    # pickle_save_file = os.path.join(savedir, 'populated_df.pkl')
    # df.to_pickle(pickle_save_file)
    #
    # writer = pd.ExcelWriter(os.path.join(savedir,'matched_reference_nodes.xlsx'))
    # df.to_excel(writer)
    # writer.save()
    #
    # print(df)

