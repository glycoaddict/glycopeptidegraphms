# -*- mode: python -*-

block_cipher = None


a = Analysis(['convert-progenesis.py'],
             pathex=['C:\\Users\\choom.000\\Documents\\glycopeptidegraphms_public\\graphms\\fileconversions'],
             binaries=[],
             datas=[],
             hiddenimports=[],
             hookspath=[],
             runtime_hooks=[],
             excludes=['matplotlib', 'scipy'],
             win_no_prefer_redirects=False,
             win_private_assemblies=False,
             cipher=block_cipher)
pyz = PYZ(a.pure, a.zipped_data,
             cipher=block_cipher)
exe = EXE(pyz,
          a.scripts,
          a.binaries,
          a.zipfiles,
          a.datas,
          name='convert-progenesis',
          debug=False,
          strip=False,
          upx=True,
          runtime_tmpdir=None,
          console=True )
