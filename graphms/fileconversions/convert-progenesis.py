"""
this takes a csv outputted by progenesis QI
and converts it to a consensusxml format
that can act as an input to GraphMS.
"""

from graphms import custom_tools
# import custom_tools # this form is needed for compiling.
import pandas as pd
import numpy as np
import os, sys
from collections import OrderedDict
import unittest
import tkinter as tk

def get_file(file_path='', delimiter='@', is_test=False):

    assert isinstance(file_path, str), 'convert-progenesis get_file received file_path not as a str = {}'.format(file_path)
    assert isinstance(is_test, bool), 'convert-progenesis get_file received is_test not as a bool = {}'.format(
        is_test)
    assert isinstance(delimiter, str), 'convert-progenesis get_file received delimiter not as a str = {}'.format(
        file_path)

    if is_test:
        file_path = r'O:\Analytics\Lab data backup\Orbitrap Fusion\2019\May\progeneis cho epo triplicates r1,2,3-2, runs separate (profile data).csv'
    elif file_path=='':
        file_path=custom_tools.getfile('Select the progenesis "compound measurements" csv file', '.csv')

    df = pd.read_csv(file_path, header=[0,1,2], index_col=None, low_memory=False)
    df.columns = [delimiter.join([val for val in triplet if 'Unnamed' not in val]) for triplet in list(df.columns)]

    return df


def generate_consensus_files_as_dict_of_dfs(df_of_progenesis_compound_measurements):

    # get columns to apply to
    list_of_cols = get_list_of_columns_that_are_sample_raw_abundances(df_of_progenesis_compound_measurements)
    list_of_col_names = get_delimited_names_from_column_list(df_of_progenesis_compound_measurements,
                                                             list(list_of_cols),
                                                             delimiter='@',
                                                             position=-1)


    # initialise the result dataframe
    df_template = pd.DataFrame(columns=['rt_cf',	'mz_cf', 'intensity_cf',
                                   'charge_cf','width_cf', 'quality_cf',
                                   'rt_0','mz_0','intensity_0','charge_0'])

    result_dict = OrderedDict()

    ## do any global preprocessing here
    # remove those charge states of 1
    df_of_progenesis_compound_measurements = df_of_progenesis_compound_measurements.loc[-(df_of_progenesis_compound_measurements['Charge'] == 1), :]


    for col, name in zip(list_of_cols, list_of_col_names):

        df_temp_result = df_template.copy(True)

        # slice out the rows where the current col is not zero in raw abundance
        temp_df_of_progenesis_compound_measurements = df_of_progenesis_compound_measurements.loc[
                                                     df_of_progenesis_compound_measurements.iloc[:,col]>0,:]

        # start populating the result df
        df_temp_result.loc[:, 'rt_cf'] = temp_df_of_progenesis_compound_measurements['Retention time (min)']*60
        df_temp_result.loc[:, 'mz_cf'] = temp_df_of_progenesis_compound_measurements['Neutral mass (Da)']
        # calculate the neutral mass if there is a blank
        df_temp_result.loc[:, 'mz_cf'] =  df_temp_result.loc[:, 'mz_cf'].fillna(
            temp_df_of_progenesis_compound_measurements['m/z']*temp_df_of_progenesis_compound_measurements['Charge']- \
            (temp_df_of_progenesis_compound_measurements['Charge']*1.00728))
        df_temp_result.loc[:, 'intensity_cf'] = temp_df_of_progenesis_compound_measurements.iloc[:,col]
        df_temp_result.loc[:, 'charge_0'] = temp_df_of_progenesis_compound_measurements['Charge']
        df_temp_result.loc[:, 'mz_0'] = temp_df_of_progenesis_compound_measurements['m/z']
        df_temp_result.loc[:, 'rt_0'] = temp_df_of_progenesis_compound_measurements['Retention time (min)']*60
        df_temp_result.loc[:, 'quality_cf'] = temp_df_of_progenesis_compound_measurements['Adducts']

        # store in dict
        result_dict[name] = df_temp_result

    return result_dict


def get_list_of_columns_that_are_sample_raw_abundances(df_in):
    """
    uses the number of columns between Normalised Abundance and Raw Abundance to detect the sample names
    Use those sample names to collect the correct columns, as integer list (not names because of duplication)

    Gets only the Raw Abundance, not the Normalised Abundance.

    Uses the number of detected samples to choose the number of columns of raw data to get.

    :param df_in:
    :return:
    """

    first_marker_column_index = np.argwhere(['Normalised abundance' in x for x in df_in.columns])[0][0]
    last_marker_column_index = np.argwhere(['Raw abundance' in x for x in df_in.columns])[0][0]
    count_of_columns_with_samples = last_marker_column_index - first_marker_column_index

    list_of_cols = list(range(last_marker_column_index, last_marker_column_index+count_of_columns_with_samples))

    return list_of_cols


def get_delimited_names_from_column_list(df, column_list, delimiter='@', position=-1):
    # check inputs
    assert isinstance(df,
                      pd.DataFrame), 'get_delimited_names_from_column_list received df as not a pd.DataFrame = {}'.format(
        type(df))
    assert isinstance(column_list,
                      list), 'get_delimited_names_from_column_list received column_list as not a list = {}'.format(
        type(column_list))
    assert isinstance(delimiter,
                      str), 'get_delimited_names_from_column_list received delimiter as not a string = {}'.format(
        type(delimiter))

    assert isinstance(position,
                      int), 'get_delimited_names_from_column_list received position as not a integer = {}'.format(
        type(position))

    # slice the names
    column_names = df.columns[column_list]

    # split the names, taking the last position
    splitted_names = [name.split(delimiter)[position] if '@' in name else 'no_delimiter_{}'.format(name) for name in column_names]

    assert len(splitted_names)==len(column_list), 'len(Splitted columns)={} does not match input len(column_list)={}'.format(len(splitted_names,len(column_list)))

    return splitted_names


def save_dict_of_dfs_as_csvs(dict_of_dfs, savedir):

    assert isinstance(dict_of_dfs, dict), 'convert-progenesis save_dict_of_dfs_as_csvs received dict_of_dfs not as a dict = {}'.format(dict_of_dfs)
    assert isinstance(savedir,
                      str), 'convert-progenesis save_dict_of_dfs_as_csvs received savedir not as a str = {}'.format(
        savedir)

    for k in dict_of_dfs.keys():
        assert isinstance(dict_of_dfs[k], pd.DataFrame), 'dict_of_dicts[{}] not a dataframe = {}'.format(k, dict_of_dfs[k])
        save_df_as_csv(dict_of_dfs[k], os.path.join(savedir, k + '.csv'))

    custom_tools.opendir(savedir)


def save_df_as_csv(df, file_name):
    df.to_csv(file_name, sep=',', na_rep='0', index=None)
    print('Saved dataframe of shape {} to file {}.'.format(df.shape, file_name))


class Test(unittest.TestCase):
    def testmodules(self):

        ans = pd.Index(['Compound', 'Neutral mass (Da)', 'm/z', 'Charge',
                        'Retention time (min)', 'Chromatographic peak width (min)',
                        'Identifications', 'Max Fold Change', 'Highest Mean', 'Lowest Mean',
                        'Isotope Distribution', 'Maximum Abundance', 'Minimum CV%',
                        'Normalised abundance@Condition 1@a',
                        'Condition 2@b',
                        'Condition 3@c',
                        'Condition 4@d',
                        'Condition 5@e',
                        'Condition 6@f',
                        'Raw abundance@Condition 1@a',
                        'Condition 2@b',
                        'Condition 3@c',
                        'Condition 4@d',
                        'Condition 5@e',
                        'Condition 6@f',
                        'Accepted Compound ID', 'Accepted Description', 'Adducts', 'Formula',
                        'Score', 'Fragmentation Score', 'Mass Error (ppm)',
                        'Isotope Similarity', 'Retention Time Error (mins)', 'Compound Link'],
                       dtype='object')

        ans2 = [x for x in 'abcdef']

        test_get_file = get_file(file_path='testfile.csv', delimiter='@', is_test=False)

        self.assertTrue(np.all(test_get_file.keys()==ans), 'Error in module : get_file. Output was {}\nError in comparison was at {}'.format(
            test_get_file.keys(), np.argwhere(~(test_get_file.keys()==ans)))
                         )

        test_dict_of_dfs = generate_consensus_files_as_dict_of_dfs(test_get_file)

        self.assertTrue(np.all(list(test_dict_of_dfs.keys())==ans2), 'Error in module : generate_consensus_files_as_dict_of_dfs. Output was {}'.format(test_dict_of_dfs.keys()))

        print('***Input/Output tests completed with no errors.***')


        return


class gui:
    def __init__(self):
        print('Started the GUI for convert-progenesis. Look to the left of the screen for the GUI window')
        top = tk.Tk()
        self.top = top
        top.title = 'Convert Progenesis to Consensusxml.csv'
        top.configure(background='white')

        b = tk.Button(top, text='Click to run Convert Progenesis', command=self.button_convert_progenesis)
        b.pack(pady=10)

        # set flashing color
        self.flashtracker = 0
        self.flash()

        top.mainloop()

    def flash(self):
        if self.flashtracker > 6:
            return
        self.top.configure(background='red')
        self.top.after(250, self.revert)
        self.flashtracker += 1

    def revert(self):
        self.top.configure(background='white')
        self.top.after(250, self.flash)

    def button_convert_progenesis(self):
        execute()


def execute_with_gui():
    g = gui()


def execute():
    df_of_progenesis_compound_measurements = get_file(is_test=False)
    dict_of_dfs = generate_consensus_files_as_dict_of_dfs(df_of_progenesis_compound_measurements)

    save_dict_of_dfs_as_csvs(dict_of_dfs, savedir=custom_tools.getfolder('choose output folder'))

if __name__ == '__main__':

    # unittest.main()

    # execute()

    execute_with_gui()




    # save_df_as_csv(cho, r'O:\Analytics\Matt\042 EPO christine\May_2019\CHO consensusxml.csv')
    # save_df_as_csv(hek, r'O:\Analytics\Matt\042 EPO christine\May_2019\HEK consensusxml.csv')

