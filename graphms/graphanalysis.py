"""
The main function is dographanalysis
"""

import configparser
import logging
import os
import pdb
import shutil
import time
from datetime import datetime
from threading import Thread
from tkinter import messagebox as tkmessagebox

import networkx as nx
import numpy as np
import pandas as pd

from graphms import custom_tools, linksubgraphs, glymod_functions, cleanconsensus


def log_now(*args):
    m = [str(datetime.now()) + ' >>> ']
    m.extend(list(args))
    n = ' '.join(map(str, m))

    t = Thread(target=update_status, args=[n])
    t.start()
    t.join()
    logging.info([n])


def timedifference(a, b):
    m, s = divmod(b - a, 60)
    h, m = divmod(m, 60)
    return ('{:02.0f}:{:02.0f}:{:02.6f}'.format(h, m, s))


cnames = custom_tools.cnames()


###### Functions

def mask_array_match_all_with_or(mask_array):
    # take the first item out first.
    # then OR match everything else with it.
    mask_with_global_or_match = mask_array[0]
    for mask_index in range(1, len(mask_array)):
        mask_with_global_or_match = (mask_with_global_or_match) | (mask_array[mask_index])
    return mask_with_global_or_match


def update_status_threaded(*args):
    m = [str(datetime.now()) + ' >>> ']
    m.extend(list(args))
    n = ' '.join(map(str, m))

    t = Thread(target=update_status, args=[n])
    t.start()
    t.join()


def update_status(s):
    print(s)


def dographanalysis(inipath='', consensuspath='', jumplistpath='', outputfolderpath=''):
    ###### control parameters consolidated here:
    config = configparser.ConfigParser()
    if len(inipath) < 1:
        inipath = custom_tools.getfile('Choose GraphAnalysis parameters .INI file', '.ini')
        if len(inipath) < 1:
            print('Error. no ini settings file was provided')
            return False
    config.read(inipath)

    # must force them to the data types, otherwise it will be parsed as Unicode32 <U32 and mess up any operations
    sizemax = int(config.get('Graph_parameters', 'sizemax'))
    intensity_threshold = float(config.get('Graph_parameters', 'intensity_threshold'))
    toleranceDa = float(config.get('Graph_parameters', 'toleranceDa'))
    degreemax = int(config.get('Graph_parameters', 'degreemax'))
    plot_node_connectivity = int(config.get('Graph_parameters', 'plot_node_connectivity'))
    use_weight_limit = float(config.get('Graph_parameters', 'use_weight_limit'))
    rt_use_csv_diff = bool(config.getboolean('Graph_parameters', 'rt_use_csv_diff'))
    chunk_size = int(config.get('Graph_parameters', 'chunk_size'))
    neuac_percent_match = float(config.get('Graph_parameters', 'neuac_percent_match'))

    ###### Run time program below here


    # jumplist is the .csv list of glycan differences you want to match, and their composition and maximum RT difference to allow a match.
    # the weight column allows one to assign a weight to the connection(edge) and limit the kinds of edges that can be joined to one node.
    # headers should be: Difference	N	F	H	S	max_rt	weight
    if len(jumplistpath) < 1:
        jumplistpath = custom_tools.getfile('Choose CSV file containing glycan differences', '.csv')
        #    sJumpList = custom_tools.getfile('Choose CSV file containing glycan differences')
        if len(jumplistpath) < 1:
            log_now('no jumplist file was selected!')
            return False

    # prompt user for input files.
    # consensusxml.csv file must be .csv output from OpenMS/TOPPAS ConsensusXML->TextFileExporter
    # headers should be: rt_cf	mz_cf	intensity_cf	charge_cf	width_cf	quality_cf	rt_0	mz_0	intensity_0	charge_0	width_0
    #
    # check if a file needs to be loaded
    print('consensuspath=', consensuspath)
    if len(consensuspath) < 1:
        consensuspath = custom_tools.getfile('Choose OpenMS consensusxml.csv file')
        if len(consensuspath) < 1:
            log_now('no data file was selected!')
            return False
    [consensuspath] = cleanconsensus.clean([consensuspath])

    # the output directory for all the graphml and png files. Recommended to use a new folder for each sample as it will overwrite.
    savefolder = outputfolderpath or custom_tools.getfolder('Choose output folder for graphml files')

    if len(savefolder) < 1:
        log_now('no output folder was selected!')
        return False

    # trace
    time_begin = time.time()  # collects timestamp
    log_now('GraphAnalysis.py began')

    # custom_tools.standard if output directory exists, if not, create.
    directory = os.path.dirname(savefolder)
    if not os.path.exists(directory): os.mkdir(directory)

    # for better traceability, make a copy of the parameters and the consensus file.
    for p in [inipath, jumplistpath, consensuspath]:
        try:
            shutil.copy(p, savefolder)
        except shutil.Error:
            pass

    time_fileload_begin = time.time()

    df = pd.read_csv(consensuspath, header=0,
                     dtype={0: np.float,
                            1: np.float,
                            2: np.float,
                            9: np.int
                            })
    df = custom_tools.checkcolumnsandreplace(df)

    glymods = glymod_functions.glycan_mods(jumplistpath, 'glymod_')

    time_fileload_done = time.time()  # collects timestamp

    # first, isolate the df/consensuspath file to only have those above the intensity threshold.
    intensity_mask = (df.loc[:, ['intensity_cf']].values >= intensity_threshold)
    # df = df[intensity_mask].reset_index(drop=True)

    # and also the RT and mz limits
    # intensity_mask = intensity_mask # & (df.ix[:,'mz_cf':'mz_cf'].values >= mz_limit[0]) & (df.ix[:,'mz_cf':'mz_cf'].values <= mz_limit[1]) & (df.ix[:,'rt_cf':'rt_cf'].values >= rt_limit[0]) & (df.ix[:,'rt_cf':'rt_cf'].values <= rt_limit[1])
    df = df[intensity_mask].reset_index(drop=True)

    ############## here, need to move up the index by 1
    #######################################################
    df.index += 1
    ####################################################

    # df = df[('rt_cf' >= rt_limit[0]) & ('rt_cf' <= rt_limit[1])]

    # log_now(df)

    """
    Begin adding to graph here
    """

    # loads features into the nodes as index=0,1,2,3... with attribute = dictionary of feature properties
    G = nx.Graph()

    # calculate dimensions of the CSV files
    iter_csv = df.itertuples(name='Feature')  # for the data list
    isize = (df.count(axis='index')[0])

    # Issue a warning if there are many data points, although since the speed improvements this is less of an issue.
    max_size = 16000
    if isize > max_size:
        strwarning = 'The selected consensus data file has ' + str(isize) + ' nodes. Processing more than ' + str(
            max_size) + ' nodes will take some time, and your computer may be slowed during processing. If system messages cease and your computer hangs, you may need to restart the computer. \n DO YOU STILL WISH TO PROCEED?'
        print(str(datetime.now()), strwarning)
        log_now(strwarning)
        if not tkmessagebox.askyesno('WARNING', strwarning):
            log_now('Program exited upon user command; too many feature nodes encontered')
            return False

    maxjump = max(
        glymods.df['difference']) + toleranceDa  # gets the largest jump in jumplist and adds the tolerance to it
    minjump = min(glymods.df['difference']) - toleranceDa
    maxRTjump = max(glymods.df['max_rt'])

    # NB: intensity filters should be set in the source data CSV file, not here! It's not compatible with a filter yet because then the nodes would be differently indexed to the subtractions.
    # i could instead sequentially number the nodes as they go in, then extract their mz back out for the subtraction table, thus preserving the index.

    print("Node extraction begun...")

    # collect time stamp.
    time_node_adding_begin = time.time()

    # range doesn't include the final item, hence needing +1
    # df.index[0] is used to anchor the loop to the first node index
    # in case it is 0 OR 1.    
    for i in range(int(df.index[0]), int(df.index[-1]) + 1):
        data_row = next(iter_csv)
        # log_now('data_row = ', data_row)


        G.add_nodes_from([(data_row[0], {'rt': float(data_row[1]),
                                         'mz': float(data_row[2]),
                                         'intensity': float(data_row[3]),
                                         'precursor_mz': float(data_row[8]),
                                         'precursor_rt': float(data_row[7]),
                                         'precursor_charge': int(data_row[10]),
                                         'composition': '',
                                         'backbone': '',
                                         })])

    update_status_threaded('Node extraction finished')
    time_node_adding_done = time.time()  # collects timestamp
    dfcount = df.count(axis='index')[0]
    log_now('size of the LCMS features, df.count = ', dfcount)  # this shows the size of the dataframe.

    # (3) connecting nodes with differences corresponding to the list of jumps.
    # (3a) First, get an indexed list of the m/z values. They must be indexed with the exact index used in the dataframe.
    # So, just slice the column!
    # sizemax makes sure the array is not more than the value specified in the INI file [default=10,000]
    if sizemax > 0:
        mz_slice = df.loc[:sizemax, ['mz_cf']].values  # this gets the slice of the m/z values as the indexed 0: m/z
        rt_slice = df.loc[:sizemax, ['rt_cf']].values
    else:
        mz_slice = df.loc[:, ['mz_cf']].values  # this gets the slice of the m/z values as the indexed 0: m/z
        rt_slice = df.loc[:, ['rt_cf']].values

        # pad with a starting zero so that it will match my indices
    mz_slice = np.insert(mz_slice, 0, 0., axis=0)
    rt_slice = np.insert(rt_slice, 0, 0., axis=0)

    # (3b) Then, do a matrix subtraction such that I end up with every single combination of differences.
    # this subtracts the array from its transposed array to yield a 2D array.
    # as a numpy array = [[(u, v), subtraction],...,]


    no_of_slices = int(np.ceil(float(len(mz_slice)) / chunk_size))
    log_now('number of slices = ', no_of_slices)
    zipped_array = np.array([[]])

    if True:
        slice_ticker = 0
        for x in range(1, no_of_slices + 1):
            for y in range(1, no_of_slices + 1):

                slice_ticker += 1
                if slice_ticker > (no_of_slices ** 2): break
                log_now('active slice number = ', slice_ticker)
                # the range must start from 1 onwards, otherwise (x-1) below will make it index from the end of the array!
                slice_x_vals = mz_slice[((x - 1) * chunk_size):(x * chunk_size) - 1]
                slice_y_vals = mz_slice[((y - 1) * chunk_size):(y * chunk_size) - 1].T
                mz_combine = np.subtract(slice_x_vals, slice_y_vals)

                ##        log_now('active slice of mz_combine = \n', mz_combine)

                slice_p_vals = rt_slice[((x - 1) * chunk_size):(x * chunk_size) - 1]
                slice_q_vals = rt_slice[((y - 1) * chunk_size):(y * chunk_size) - 1].T
                rt_combine = np.subtract(slice_p_vals, slice_q_vals)
                #                rt_combine = np.insert(rt_combine, 0, 0.)
                ##        log_now('active slice of rt_combine = \n', rt_combine)

                ### create several masks based on conditions

                # restrict to the min and max differences found in the JumpList
                # the intersect where both conditions are True, as a Mask
                # m3
                mask_mz_window = (((mz_combine >= minjump) & \
                                   (mz_combine <= maxjump)) & \
                                  (mz_combine >= 0))

                # restrict to the maximum RT difference found in the JumpList
                # m5
                mask_rt_window = ((rt_combine <= maxRTjump) & (rt_combine >= -maxRTjump))

                # create a mask based on the jumplist and tolerance
                # mask
                mask_array_mz_with_tolerance_from_jumplist = []

                # do an AND match for all the different values in the JumpList
                # this gets the differences that correspond to a Jump.
                for current_jump_mz in glymods.df['difference']:
                    mask_array_mz_with_tolerance_from_jumplist.append(
                        (mz_combine >= (current_jump_mz - toleranceDa)) & \
                        (mz_combine <= (current_jump_mz + toleranceDa)) \
                        )

                    # this sets the first item of mask2, then the second
                    # item is OR matched and so on,
                    # using the loop.
                #                mask2 = mask_array_mz_with_tolerance_from_jumplist[0]
                #                pdb.set_trace()
                #                for r in range(1, len(mask_array_mz_with_tolerance_from_jumplist)):
                #                    mask2 = (mask2) | (mask_array_mz_with_tolerance_from_jumplist[r])

                # this sets the first item of mask2, then the second
                # item is OR matched and so on,
                # using the loop.
                # mask2
                # this uses a custom function.
                mask_concat_all_or_matched = mask_array_match_all_with_or(mask_array_mz_with_tolerance_from_jumplist)

                # then do an AND match to the mz and RT restrictions
                # the AND match ensures that anything outside the [mz, RT] bounds is discarded
                mask_concat_all_or_matched = (mask_concat_all_or_matched & (mask_mz_window & mask_rt_window))

                # get the indices!
                # m4 are the indices of the 
                # returns a two-tuple of ( array[row indices], 
                # array[column indices] ), refer using m4[0] and m4[1]
                # this gets the indexes of the nonzero positions (i.e. True)
                # from mask_concat_all_or_matched
                # m4:
                indices_of_mask_concat = np.array(np.nonzero(mask_concat_all_or_matched), dtype=np.uint64)
                # because I used chunks, I now need to add a value to each so that these indices give me the actual nodes



                log_now('total number of differences to match to jumplist; shape of m4/indices_of_mask_concat = ',
                        indices_of_mask_concat.shape)

                working_zipped_array = np.array(np.vstack(
                    [indices_of_mask_concat[0] + ((x - 1) * chunk_size),
                     indices_of_mask_concat[1] + ((y - 1) * chunk_size),
                     mz_combine[mask_concat_all_or_matched]]).T, dtype=np.float_)
                try:
                    zipped_array = np.vstack((zipped_array, working_zipped_array))
                    log_now('zipped_array was appended')
                except:
                    zipped_array = working_zipped_array
                    log_now('zipped_array was initiated')
                    #    except:
    # pdb.set_trace()


    log_now('shape of zipped_array = ', zipped_array.shape)
    log_now('zipped_array = \n', zipped_array)

    #### end of chunking loop should go here

    ### need to concatenate zipped_array.



    # now go through small chunks of the data.
    # chunk_size = 10000
    total_items = zipped_array.shape[0]
    number_of_chunks = np.int(np.ceil(total_items / chunk_size))
    log_now('number of chunks = ', number_of_chunks)

    # this is the highest loop, that just goes through the chunks
    # i'd want to save the graphml each time, then maybe stitch it together at the end?

    # first, initialise all the important inputs
    #        intensity_nodes = nx.get_node_attributes(G,'intensity') # this creates a list of intensities for each node.
    rt_nodes = nx.get_node_attributes(G, 'rt')  # this creates a list of intensities for each node.
    #        mz_nodes = nx.get_node_attributes(G,'mz') # this creates a list of intensities for each node.
    ticker = 0
    matches = 0
    ### start adding edges here
    time_edge_adding_begin = time.time()

    for active_chunk in range(0, (number_of_chunks)):

        # get the subset of the zipped_array
        start_pos = (active_chunk) * chunk_size + 1  # +1 is here because my indices now start from 1, instead of 0
        end_pos = (active_chunk + 1) * (chunk_size)

        for index1, index2, val in zipped_array[start_pos:end_pos]:
            # print('index1, index2, val = {},    {},    {}'.format(index1,index2,val))
            index = [int(index1), int(index2)]
            # make the match
            ticker += 1
            if ticker % 1000 == 1: log_now(
                'Edge loop being processed: {:10.4f}% , {} / {}'.format(
                    np.multiply(np.divide(ticker, total_items), 100), ticker,
                    total_items))

            # this works really well to see if there is a 0 when you round the (val - jumplist) as a shortcut; ie a match
            ##        checkarray = np.rint(np.subtract(val, np.array(df_jumplist['Difference'])))
            checkarray = np.isclose(np.subtract(val, np.array(glymods.df['difference'])), 0.0, atol=toleranceDa)

            # this next part tests for if not 0 in checkarray: continue            
            pos = []  # here just in case it didn't get cleared from the previous cycle
            # this next line gets the position of the zero (i.e match) as [n], 
            # so the pos[0] is needed.
            pos = np.where(checkarray == True)[0]

            # if there is no match, pos will be an empty array, and len(pos[0])==0
            # note that len(pos) = 2 because then just checks the size of the array.
            # i caught a bug here. in the case where pos=[0], i.e. matches the first
            # element in Difference, it would be seen as False! 
            if len(pos) == 0:
                continue
            else:
                # apply conditions
                try:
                    if nx.has_path(G, index[0], index[1]):
                        if (nx.degree(G, index[0]) >= degreemax) or (nx.degree(G, index[1]) >= degreemax):
                            continue  # stops if either node is already too connected
                        if (use_weight_limit > 0) and \
                                (nx.shortest_path_length(G, index[0], index[1], weight='weight') <= use_weight_limit):
                            continue
                except:
                    pdb.set_trace()
                    continue
                    # stops if there is already a path, to reduce visual complexity, but only if its not made of 2 long jumps > 500 each

                # but if there is NO PATH to the target nodes, continue on to draw the edge.




                ### now that conditions have all passed, start to match edges.
                # hit slice is the row slice from the difference list, having all the same columns.
                hit_slice = (glymods.df[(pos[0]):(pos[0] + 1)])

                diff = np.fabs(val - np.float(hit_slice['difference']))
                # print('diff = ', diff)
                if diff <= toleranceDa:
                    ## get the RT difference between nodes 
                    delta_rt = (float(np.subtract(rt_nodes[index[1]],
                                                  rt_nodes[index[0]])))
                    rt_diff_csv = float(hit_slice['max_rt'])

                    if (rt_use_csv_diff) and (np.fabs(delta_rt) >= rt_diff_csv):
                        # if the match falls outside the RT limits set in the CSV file.
                        log_now('RT difference was exceeded, [Da, RT] = ', val, delta_rt)
                        continue

                    else:
                        my_attr_dict = {}
                        my_attr_dict['delta_m/z'] = float(val)
                        my_attr_dict['delta_RT'] = float(delta_rt)
                        my_attr_dict['hypotenuse'] = 1
                        my_attr_dict['weight'] = int(hit_slice['weight'])
                        for col_names in glymods.columns:
                            my_attr_dict[col_names] = int(hit_slice[col_names])

                        G.add_edge(index[1], index[0])
                        G.edges[index[1], index[0]].update(my_attr_dict)

                        matches += 1

    time_edge_adding_done = time.time()  # collects timestamp

    time_savingdrawing_begin = time.time()  # collects timestamp    

    time_savingdrawing_done = time.time()  # collects timestamp

    log_now('Began splitting into subgraphs')

    # split the graph into subgraphs
    graphs = sorted(nx.connected_component_subgraphs(G, copy=True), key=len, reverse=True)
    log_now('Total of subgraphs before culling = ', str(len(graphs)))

    s = []  # the list which will take all the subgraphs appended onto it.
    # col = ['r', 'b', 'g', 'c', 'm', 'k', 'coral', 'aqua', 'brown', 'crimson', 'maroon', 'darkgoldenrod', 'teal' ] # this sets the color list, of which a modulus in len(c) will provide a cyclic 0-5

    ####### DEFINE THE BOKEH GRAPH HERE

    # populate a list for the composition based on glycan mods given 
    # in jumplist    
    comp_string = ""
    for v in glymods.columns:
        comp_string += v[len("glymod_"):]
        comp_string += "@"
        comp_string += v + " "

    graphpath_list = []  # makes a list of the graphml paths for neuac joining later

    for i, c in enumerate(graphs):
        if len(c) <= plot_node_connectivity:
            if i == 0: print("no edges were connected at all!")
            break



            # this saves the subgraph to file as graphml
        # this creates out of G graph, the subgraph s from the nbunch c (c from the extracted connected subgraphs)
        s.append(G.subgraph(c).copy())
        path_graphml = os.path.abspath(
            os.path.join(savefolder, 'subgraphs' + str(i) + '_degree' + str(len(c)) + '.graphml'))

        ### add the metadata node
        #

        s[i].add_nodes_from([(0, {'jumplist_all': (glymods.df_all).to_html(index=False),
                                  'jumplist_cluster': (glymods.df_cluster).to_html(index=False),
                                  'jumplist': (glymods.df).to_html(index=False),
                                  'rt': 0.0,
                                  'mz': 0.0,
                                  'intensity': 0.0,
                                  })])

        # write the subgraph to disk as a .graphml
        nx.write_graphml(s[i], path=path_graphml)
        graphpath_list.append(path_graphml)
        log_now('Bokeh : added and saved subgraph ', i, ' to ', path_graphml)

    outgraphpathlist = graphpath_list

    #######################################################
    ############## do neuac matching first ################
    #######################################################
    if neuac_percent_match > 0.0:
        tolink = linksubgraphs.linksub(graphmls=graphpath_list,
                                       outputfolder=savefolder,
                                       threshold=neuac_percent_match,
                                       df_cluster=glymods.df_cluster)

        # escape if no matches were found, in which case
        # tolink is returned as False.
        if tolink == False:
            print('no matches were found.')
            groupedgraphs = s  # is the list containing all subgraphs before matching
        else:
            #        tolink.execute()
            # get the final set of neuac grouped graphs
            groupedgraphs = tolink.get_outgraphs()
            #         sort them via size
            groupedgraphs = sorted(groupedgraphs, key=len, reverse=True)

            outgraphpathlist = tolink.get_outgraphlist()
    else:
        # if neuac matching not specified, then
        # set groupedgraphs to graphs to ensure
        # continuity with the code.
        groupedgraphs = s

    time_done = time.time()
    # make a list of the time taken to run different parts of this program.
    log_now('Timing of runtime: \n Began @ {}, Completed @ {}, Elapsed = {} \n '.format(time.ctime(time_begin),
                                                                                        time.ctime(
                                                                                            time_savingdrawing_done),
                                                                                        timedifference(time_begin,
                                                                                                       time_savingdrawing_done)))
    log_now('Time to load files = ', timedifference(time_fileload_begin, time_fileload_done))
    log_now('Time to add nodes = ', timedifference(time_node_adding_begin, time_node_adding_done))
    log_now('Time to add edges = ', timedifference(time_edge_adding_begin, time_edge_adding_done))
    log_now('Time to save graph = ', timedifference(time_savingdrawing_begin, time_savingdrawing_done))
    log_now('Time to draw bokeh graph = ', timedifference(time_savingdrawing_done, time_done))
    log_now('GraphAnalysis finished')

    # now, return as a dict all the outputs

    result = dict(savefolder=savefolder,
                  outgraphpathlist=outgraphpathlist,
                  )
    # logfilepath=logfilepath,

    return result


if __name__ == "__main__":
    dographanalysis()
