# -*- coding: utf-8 -*-
"""
Created on Tue Nov 14 15:54:38 2017

@author: choom
"""
import pandas as pd
import numpy as np


"""
scaleintensity takes a series and outputs values between [0,1]
"""

def scaleintensity(df, mode=1, forcedscale=(0,0)):


    # if no/default scaling factor was passed, then calculate the values
    def generate_scaling_factor(forcedscale, minval, maxval):
        if forcedscale ==(0,0):
            return (minval, maxval)
        else:
            return (forcedscale[0], forcedscale[1])

    def doscale(df, forcedscale):
        print('df=\n', df.iloc[0:5])
        print('forced scale=\n', forcedscale)
        g = generate_scaling_factor(forcedscale, df.min(), df.max())
        print('generated scaling factor=\n', g)
        df_scaled_values = (df - g[0]) / (g[1]-g[0])
        print('scaled values=\n', df_scaled_values.iloc[0:5])
        return df_scaled_values


    try:
        if not isinstance(df, pd.Series):
            df = pd.Series(df)
    except:
        print('Error. scaleintensity input could not be converted to a pandas Series')
        return df
    #dfx['intensity_scaled'] = ((np.log10(dfx['intensity']) - 6) / 6) ** (2) * (circle_size_max - circle_size_min) + circle_size_min


    if mode==0:
        return ((np.log10(df) - 6) / 6) ** (2)
    
    elif mode==1:
#        u = df.mean(axis=0, skipna=True, numeric_only=True)
#        s = df.std(axis=0, skipna=True, numeric_only=True)
        
        df = np.log10(df)
        # this gets the upper 75% threshold        
        q = df.quantile(q=0.25, interpolation='midpoint')
        # below the q threshold, transform all to the miniumum value, i.e. q
        df[df<=q] = q
        # normalise by min and max to make it over [0,1]
        df = doscale(df, forcedscale)
        return (df)
    
    elif mode==2:
        # this mode sets it to the 25% to 90% mark
        df = np.log10(df)        
        qmin = df.quantile(q=0.15, interpolation='midpoint')
        qmax = df.quantile(q=0.85, interpolation='midpoint')
        df[df<=qmin] = qmin
        df[df>=qmax] = qmax
        df = doscale(df, forcedscale)
        return (df)
    
    elif mode==3:
        # this just does a simple normalisation after log
        df = np.log10(df)
        # df = doscale(df, forcedscale)
        return df

    elif mode==4:
        # log then just sqrt
        df = np.sqrt(df)
        df = doscale(df, tuple(np.power(10, forcedscale)))
        return df






    
"""
main body here
""" 
if __name__ == "__main__":
    a = sorted(np.random.rand(100)*10)
    b = scaleintensity(a,0)
    c = scaleintensity(a,1)
    d = scaleintensity(a,2)
    
    import matplotlib.pyplot as plt
    
#    ax = plt.plot(a, label='a')
    bx = plt.plot(b, label='b')
    cx = plt.plot(c, label='c')
    dx = plt.plot(d, label='d')
    plt.legend()
