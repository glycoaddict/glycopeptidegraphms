.. graphms documentation master file, created by
   sphinx-quickstart on Mon Nov 25 17:45:38 2019.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

===================================
GlycopeptideGraphMS documentation
===================================

Info
====

* **Author:** Matthew SF CHOO, Ph.D.
* **Contact:** choo dot matt at gmail dot com
* **Citation:** Choo, M.S., Wan, C., Rudd, P.M., Nguyen-Khuong, T., 2019. GlycopeptideGraphMS: Improved Glycopeptide Detection and Identification by Exploiting Graph Theoretical Patterns in Mass and Retention Time. Anal. Chem. 91, 7236–7244. https://doi.org/10.1021/acs.analchem.9b00594
* **Repo:** https://bitbucket.org/glycoaddict/glycopeptidegraphms/
* **Binaries:** the Win-64bit binaries are at https://bitbucket.org/glycoaddict/glycopeptidegraphms/downloads/


Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`


Contents
========

.. toctree::
   :maxdepth: 6

   modules








